import socket
import Globals
import multiprocessing
import ConnHandler
import aioprocessing
import Debug


class NewConn:
	def __init__(self, aSocket, ip):
		self.socket = aSocket
		self.ip = ip

class Listener:

	def __init__(self):
		self.conns_queue = aioprocessing.AioQueue()
		self.conns_queue_lck = aioprocessing.AioLock()
		self.conns_queue_cv = aioprocessing.AioCondition(self.conns_queue_lck)
		self.nodes = aioprocessing.AioManager().dict()
	def listen(self):
		#NewConnTuple = namedtuple('NewConnTuple', 'socket ip')
		processes = []
		conns_storage = []
		conn_handler = ConnHandler.ConnHandler(self.conns_queue,  self.conns_queue_cv, self.conns_queue_lck, self.nodes)
		core_count = multiprocessing.cpu_count()
		for _ in range(core_count):
			processes.append(aioprocessing.AioProcess(target=conn_handler.start))
			processes[-1].start()
		host = socket.gethostname()
		listen_socket = socket.socket()  # get instance
		listen_socket.bind(("", Globals.DS_PORT))  # bind host address and port together
		while True:
			try:
				listen_socket.listen()
				sock, address = listen_socket.accept()  # accept new connection
				ip = socket.inet_aton(address[Globals.IP_IN_ADDRESS])
				conns_storage.append(NewConn(sock,ip))#todo remove this
				self.add_val_to_shared_queue(conns_storage[-1])
			except Exception:
				self.add_val_to_shared_queue(False)#signals processes to shut down.
				break
		for process in processes:
			process.join()
		try:
			listen_socket.close()
		except Exception: pass

	def add_val_to_shared_queue(self, val):
		Debug.mpl.info("add_val_to_shared_queue")
		self.conns_queue_lck.acquire()
		self.conns_queue.put(val)
		self.conns_queue_cv.notify()
		self.conns_queue_lck.release()


if __name__ == '__main__':
	listener = Listener()
	listener.listen()