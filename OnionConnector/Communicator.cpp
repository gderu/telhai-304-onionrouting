#include "Communicator.h"

bool Communicator::isLastPart(std::vector<byte> msg)
{
	return (bool)(Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::FINAL_FRAGMENT_FLAG));
}

inline NODE_NAMES Communicator::getOrigin(std::vector<byte> msg)
{
	if (Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::IS_GUARD)){
		return NODE_NAMES::GUARD_NODE;
	}
	else if (Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::IS_MID)) {
		return NODE_NAMES::MID_NODE;
	}
	else {
		return NODE_NAMES::EXIT_NODE;
	}
}

Communicator::Communicator(std::mutex& outQueueMtx, std::condition_variable& outQueueCv, std::queue<InterThreadMsg*>& outQueue)
	: _outQueueMtx(outQueueMtx), _outQueue(outQueue), _outQueueCv(outQueueCv)
{
	_recvThread = nullptr;
}
Communicator::~Communicator()
{
	if (_connectSocket != INVALID_SOCKET) {
		_toShutDown = true;
		shutdown(_connectSocket, SD_SEND);
		if (_recvThread != nullptr) {
			_recvThread->join();
			delete _recvThread;
			_recvThread = nullptr;
		}
		closesocket(_connectSocket);
		WSACleanup();
	}
}

void Communicator::connectToServer()
{
	ICommunicator::connectToServer();
	_recvThread = new std::thread(&Communicator::receiveThread, this);
}

void Communicator::setCommunicationDetails(std::vector<byte> ip)
{	
	ICommunicator::setCommunicationDetails(ip, DEFAULT_PORT);
}


void Communicator::sendToServer(std::vector<byte> toSend, byte paddingLen, MSG_TYPES msgType, NODE_NAMES dst)
{
	if (INVALID_SOCKET == _connectSocket)
	{
		throw (UninitiatedConnection());
	}
	std::vector<byte> tempVec;
	std::vector<byte> partToSend;
	const char* arrPtr;
	int iResult;
	unsigned short finalFragLen = 0;
	byte flagsByte = 0;
	unsigned short currPart = 0;
	unsigned short maxPart = 0;
	unsigned int totalLength = 0;
	flagsByte = (byte)(toSend.size() <= (MSG_LEN - HEADERS_LEN_START));//is last msg
	flagsByte += (dst == NODE_NAMES::GUARD_NODE) << (byte)BITS_HEADER::IS_GUARD;
	flagsByte += (dst == NODE_NAMES::MID_NODE) << (byte)BITS_HEADER::IS_MID;
	partToSend.push_back(flagsByte);
	partToSend.push_back((byte)msgType);
	finalFragLen = (toSend.size() + HEADERS_LEN_START - HEADERS_LEN_LATE) % (MSG_LEN - HEADERS_LEN_LATE) + 1 - paddingLen;//adding one becuase of the flags.
	if (finalFragLen == 1) {//happens when the length is maximal ( eg a msg those len is MSG_LEN +HEADERS_LEN_START - HEADERS_LEN_LATE bytes)
		finalFragLen= MSG_LEN;
	}
	tempVec = Utilities::IntegerTypeTobigEndianBytes<unsigned short>(finalFragLen);
	partToSend.insert(partToSend.end(), tempVec.begin(), tempVec.end());
	if (MSG_LEN - HEADERS_LEN_START < toSend.size()) {
		partToSend.insert(partToSend.end(), toSend.begin(), toSend.begin() + (MSG_LEN - HEADERS_LEN_START));
	}
	else {//first and last frag
		toSend.resize(MSG_LEN - HEADERS_LEN_START, 0);
		partToSend.insert(partToSend.end(), toSend.begin(), toSend.end());
	}
	arrPtr = (const char*)partToSend.data();
	iResult = send(_connectSocket, arrPtr,(int) partToSend.size(), 0);
	if (iResult == SOCKET_ERROR) {
		int i = WSAGetLastError();
		closesocket(_connectSocket);
		WSACleanup();
		std::cout << i << std::endl;
		throw(CommunicationException("send failed", i));
	}
	totalLength = (unsigned short)toSend.size();//length of TCP message may not be greater than 256^2
	if (totalLength > 5000) {
		int i = 0;
	}
	maxPart = (unsigned short)ceil((float)(toSend.size() + HEADERS_LEN_START - HEADERS_LEN_LATE )/ (MSG_LEN - HEADERS_LEN_LATE));
	for (currPart = 1; currPart < maxPart; currPart++) {//fragmenting and adding to the sending queue.
		partToSend.clear();
		flagsByte = 0;
		flagsByte = (byte)(currPart == maxPart - 1);
		flagsByte += (dst == NODE_NAMES::GUARD_NODE) << (byte)BITS_HEADER::IS_GUARD;
		flagsByte += (dst == NODE_NAMES::MID_NODE) << (byte)BITS_HEADER::IS_MID;
		partToSend.push_back(flagsByte);
		if (Utilities::isolateBit(flagsByte, (byte)BITS_HEADER::FINAL_FRAGMENT_FLAG)) {//if the last msg
			partToSend.insert(partToSend.end(), toSend.begin() + (MSG_LEN - HEADERS_LEN_START) + (MSG_LEN - HEADERS_LEN_LATE) * (currPart - 1),
				toSend.end());
			partToSend.resize(MSG_LEN, 0);
		}
		else {
			partToSend.insert(partToSend.end(), toSend.begin() + (MSG_LEN - HEADERS_LEN_START) + (MSG_LEN - HEADERS_LEN_LATE) * (currPart - 1),
				toSend.begin() + (MSG_LEN - HEADERS_LEN_START) + (MSG_LEN - HEADERS_LEN_LATE) * (currPart - 1) + MSG_LEN - HEADERS_LEN_LATE);
		}
		arrPtr = (const char*)partToSend.data();
		iResult = send(_connectSocket, arrPtr, (int)partToSend.size(), 0);
		if (iResult == SOCKET_ERROR) {
			closesocket(_connectSocket);
			WSACleanup();
			throw(CommunicationException("send failed", WSAGetLastError()));
		}
	}
}

std::vector<byte> Communicator::receiveFromServer()
{
	if (INVALID_SOCKET == _connectSocket)
	{
		throw (UninitiatedConnection());
	}
	char buffer[MSG_LEN] = { 0 };
	short bytesReadTotal = 0;
	short bytesRead = 0;
	bytesRead = recv(_connectSocket, buffer + bytesReadTotal, MSG_LEN - bytesReadTotal, 0);//we don't want to recv bytes of a different msg.
	bytesReadTotal += bytesRead;
	if (bytesRead <= 0) {
		throw(CommunicationException("The connection with the server has been lost.", WSAGetLastError()));
	}
	return std::vector<byte>(buffer, buffer + MSG_LEN);
}


void Communicator::receiveThread()
{
	std::vector<byte> tempMsg;
	std::vector<byte> msg;
	char buffer[MSG_LEN] = { 0 };
	unsigned short bytesRead = 0;
	unsigned short bytesReadTotal = 0;
	unsigned short msgId = 0;
	NODE_NAMES origin= NODE_NAMES::NO_NODE;
	MSG_TYPES type = MSG_TYPES::REGULAR;
	NODE_NAMES tempOrigin = NODE_NAMES::NO_NODE;
	bool tempIsLastPart = false;
	byte headerLen = 0;
	int16_t lastLen = 0, lastLenNoEnryption, lengthBeforeLast;
	byte paddingLen = 0;
	std::unique_lock<std::mutex> outQueueLck(_outQueueMtx, std::defer_lock);
	int rv = 0;
	WSAEVENT readEvent = WSACreateEvent();
	rv = WSAEventSelect(_connectSocket, readEvent, FD_READ|FD_CLOSE);//If socket closes, a recv will be attempted, will fail, and that will raise an error.

	while (!_toShutDown){
		try {
			rv = WSAWaitForMultipleEvents(1, &readEvent,FALSE,WSA_INFINITE,TRUE);
			if (WSAResetEvent(readEvent) == FALSE) {
				throw(CommunicationException("Communication error!", WSAGetLastError()));
			}
			if (WSA_WAIT_EVENT_0== rv) {
				bytesRead = recv(_connectSocket, buffer+ bytesReadTotal, MSG_LEN- bytesReadTotal, 0);//we don't want to recv bytes of a different msg.
				bytesReadTotal += bytesRead;
				if (bytesRead <= 0) {
					throw(CommunicationException("The connection with the server has been lost.", WSAGetLastError()));
				}
				if (bytesReadTotal < MSG_LEN) {//if no recevied an entire fragment yet.
					continue;
				}
				else {
					bytesReadTotal = 0;
				}
				tempMsg = std::vector<byte>((byte*)buffer, (byte*)buffer + MSG_LEN);
				tempIsLastPart = isLastPart(tempMsg);
				//if a node crashsed, a new node may send a crash packet
				if (origin == NODE_NAMES::NO_NODE) {//first msg in a sequence.
					origin = getOrigin(tempMsg);
					type=(MSG_TYPES) tempMsg[(byte)HEADERS::MSG_TYPE_HEADER];
					if (type == MSG_TYPES::REGULAR) {
						int i = 0;
					}
					lastLenNoEnryption = Utilities::bigEndianBytesToIntegerType<short>(
						std::vector<byte>(tempMsg.begin() + (byte)HEADERS::LAST_FRAG_LEN, tempMsg.end()));
					lengthBeforeLast = MSG_LEN - HEADERS_LEN_START;
					
					if (!tempIsLastPart)
					{
						msg.insert(msg.begin(), tempMsg.begin() + HEADERS_LEN_START, tempMsg.end());
					}
					else {
						lastLen = CryptoPP::AES::BLOCKSIZE - (lastLenNoEnryption - HEADERS_LEN_START) % CryptoPP::AES::BLOCKSIZE;
						lastLen += lastLenNoEnryption;
						msg.insert(msg.begin(), tempMsg.begin() + HEADERS_LEN_START, tempMsg.begin() + lastLen);
					}
				}
				else if (origin != (tempOrigin = getOrigin(tempMsg))) {//means this is a server crashmsg from a new node
					origin = tempOrigin;
					lastLen = Utilities::bigEndianBytesToIntegerType<unsigned short>(
						std::vector<byte>(tempMsg.begin() + (byte)HEADERS::LAST_FRAG_LEN, tempMsg.end()));
					type = (MSG_TYPES)tempMsg[(byte)HEADERS::MSG_TYPE_HEADER];
					msg = std::vector<byte>(tempMsg.begin() + HEADERS_LEN_START, tempMsg.begin() + lastLen);
				}
				else if(tempIsLastPart) {//ending of a sequence
					lastLen = CryptoPP::AES::BLOCKSIZE - (lengthBeforeLast + lastLenNoEnryption - HEADERS_LEN_LATE) % CryptoPP::AES::BLOCKSIZE;
					lastLen += lastLenNoEnryption;
					msg.insert(msg.end(), tempMsg.begin() + HEADERS_LEN_LATE, tempMsg.begin() + lastLen);
				}
				else {//continuation && not ending of a sequence
					msg.insert(msg.end(), tempMsg.begin() + HEADERS_LEN_LATE, tempMsg.end());
					lengthBeforeLast += MSG_LEN - HEADERS_LEN_LATE;
				}
				if (tempIsLastPart) {
					paddingLen = lastLen - lastLenNoEnryption;
					if (lastLenNoEnryption < 0) {
						paddingLen -= HEADERS_LEN_LATE;//lastLen contains HEADERS_LEN_LATER as well as the data, so to get the length of the data need to subtract HEADERS_LEN_LATE
					}
					outQueueLck.lock();
					_outQueue.push(new UnprocessedMsg(origin, type,paddingLen, msg));
					outQueueLck.unlock();
					_outQueueCv.notify_all();
					msg.resize(0);
					origin = NODE_NAMES::NO_NODE;
				}
			}
		}
		catch (CommunicationException cmExc) {
			outQueueLck.lock();
			_outQueue.push(new InterThreadCommunicationException(cmExc));
			outQueueLck.unlock();
			break;
		}
	}
	WSACloseEvent(readEvent);
}



void Communicator::closeCommunication()
{
	if (_connectSocket != INVALID_SOCKET) {
		if (_recvThread != nullptr) {
			_toShutDown = true;
			shutdown(_connectSocket, SD_SEND);
			_recvThread->join();
			delete _recvThread;
			_recvThread = nullptr;
			ICommunicator::closeCommunication();
		}
		else {
			ICommunicator::closeCommunication();
		}
		_toShutDown = false;
		_connectSocket = INVALID_SOCKET;
	}
}

