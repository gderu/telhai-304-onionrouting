#include "Connection.h"

Connection::Connection(PMConnComm* comm, std::function<void(void*, ReceivedMsg, bool isAlive)> callback, void* data, uint64_t id) :_comm(comm), _callback(callback), _data(data)
{
	_id = Utilities::IntegerTypeTobigEndianBytes(id);
	_receiveMsgsThread = std::thread(&Connection::receiveMsgs, this);
}


Connection::~Connection() 
{
	endConnection();
	_receiveMsgsThread.join();
	delete _comm;
}


void Connection::sendMsg(std::vector<byte> toSend)
{
	if (_stopReceiving == true) {//checks if the connection should be over
		throw UninitiatedConnection();
	}
	toSend.insert(toSend.begin(), _id.begin(), _id.end());
	while (true) {
		try {
			_comm->pm->sendConn(new ToSendMsg(toSend));
			break;
		}
		catch (CommunicationException) {
			_comm->pm->waitForPathToBeOk();
		}
	}
}

void Connection::sendMsg(std::string toSend)
{
	sendMsg(std::vector<byte>(toSend.begin(), toSend.end()));
}

void Connection::receiveMsgs()
{
	std::unique_lock<std::mutex> commReceivedLock(_comm->receivedMtx, std::defer_lock);

	while (_stopReceiving == false) {//while it should continue
		commReceivedLock.lock();
		_comm->receivedCv.wait(commReceivedLock, [this] { return !_comm->received.empty(); });//waits until a new msg was added to the queue

		InterThreadMsg* itm = _comm->received.front();
		_comm->received.pop();
		commReceivedLock.unlock();
		if (itm->getType() == "EndConnectionIngoing") {//if the connection should be ended
			std::lock_guard<std::mutex> lck(_endConnectionMsgsMtx);
			_comm->pm->sendConn(new ConnTerminated(Utilities::bigEndianBytesToIntegerType<uint64_t>(_id)));
			_isTerminated = true;
			_stopReceiving = true;//signals to end the connection
			_callback(_data, ReceivedMsg(std::vector<byte>()), true);//calls the callback
		}
		else {
			_callback(_data, (*(ReceivedMsg*)itm), false);//calls the callback
		}
		delete itm;
	}
	std::lock_guard<std::mutex> lck(_endConnectionMsgsMtx);
	_isClosed = true;
}

void Connection::endConnection()
{
	std::lock_guard<std::mutex> lck(_endConnectionMsgsMtx);
	if (!_isTerminated) {
		_comm->pm->sendConn(new EndConnectionOutgoing(_id));
	}
}
