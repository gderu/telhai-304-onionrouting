#pragma once
#include "InterThreadMsg.h"
class CreateConn :
	public InterThreadMsg
{
	uint64_t _id;
public:
	CreateConn(uint64_t id) {
		_id = id;
	};
	uint64_t getId() { return _id; };
	virtual std::string getType(){ return "CreateConn"; };
	virtual ~CreateConn() {};
};

