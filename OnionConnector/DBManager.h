#pragma once
#include <string>
#include <exception>
#include <mutex>
#include <chrono>
#include <vector>
#include <algorithm>
#include <random>
#include <thread>
#include <io.h>
#include "sqlite3.h"
#include "Utilities.h"
#include "exceptions.cpp"
#include "PathGenerator.h"

#define NOT_EXIST -1
#define NUM_GUARDS 3

typedef struct Date {
	friend bool operator> (const Date& d1, const Date& d2);
	friend bool operator< (const Date& d1, const Date& d2);
	static Date getCurrentDate();//gets the current date

	int day, month, year;
} Date;

class DBManager {
	std::mutex _openDBMtx, _guardDataMtx;//locks when opening the database
	std::mutex _stopThreadMtx;
	bool _stopThread = false;
	std::condition_variable _stopThreadCv;
	sqlite3* _m_db;
	std::vector<std::pair<Date, std::vector<byte>>> _guardData;
	std::thread* _keepGuardNodesUpdatedThread;
	byteVec _lastGuardNodeUsed;

	void getDSIPs(std::vector<std::vector<std::string>>& DSIPs);
	void keepGuardNodesUpdatedThread();//thread that calls updateGuardNodes once every day
	void updateGuardNodes();//THROWS SqliteFailureExcpetion.
	void runLine(std::string command);//THROWS SqliteFailureExcpetion. Runs the line entered.
	bool isValidIP(std::vector<std::string> IP);
	DBManager();//THROWS ClosedDBException and SqliteFailureExcpetion
	~DBManager();
public:
	DBManager(DBManager const&) = delete;
	void operator=(DBManager const&) = delete;
	static DBManager& instance();//gets the singleton

	void addGuardNode(std::string guardIP);//THROWS SqliteFailureExcpetion. Adds a guard node with the IP entered
	std::pair<Date, std::vector<byte>> getGuardNode();//THROWS SqliteFailureExcpetion. gets a random guard node
	std::vector<byte> getDSIP();//THROWS SqliteFailureExcpetion. Gets a random data server IP
	void addDSIP(std::string DSIP);//THROWS SqliteFailureExcpetion. Adds a data server IP
	void removeGuardNode(std::string GuardIP);//THROWS SqliteFailureExcpetion.
	void removeDSIP(std::string DSIP);//THROWS SqliteFailureExcpetion.
};

int callback(void* data, int argc, char** argv, char** azColName);//callback to be used with sqlite