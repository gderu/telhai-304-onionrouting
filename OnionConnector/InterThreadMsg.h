#pragma once
#include <string>

class InterThreadMsg
{
public:
	virtual std::string getType() = 0;
	virtual ~InterThreadMsg() {};
};

