#pragma once
#include <iostream>
#include <mutex>
#include <atlbase.h>
#include "PathManager.h"
#include "ConnectionFactory.h"
#include "STCommunicator.h"
#include "DBManager.h"
#include "EncryptionUtils.h"
#define DS_PORT 43984
#define NODES_RECV_FROM_DS 2
#define SEC_IN_MILLI 1000
#define FAIL 0

enum class DS_MSGS {//no two flags can be on at once
	GIVE_NODES = 0,
	GET_NEW_NODE,
	REPORT_NODE,
	GET_GUARD_NODE
};


#define IP_LEN 4
#define TOKEN_LEN 16

typedef std::vector<byte> byteVec;
class PathGenerator
{
	std::mutex _cmMtx;
	STCommunicator _cm;
	std::mutex _primaryPathMtx;
	PathManager* _primaryPath = nullptr;//TODO remove if not neccessary
	//ipAndToken is a ref since it might be changed in the funciton!!!
	//returns false if token wrong.
	bool exchangeTokenWithNode(std::pair<byteVec, byteVec>& ipAndToken, NODE_NAMES dst, PathManager* pm,  NODE_NAMES overrideNodeFailure = NODE_NAMES::NO_NODE);
	//ipAndToken is a ref since it might be changed in the funciton!!!
	//dst is the node to communicate with (is it the mid or guard?) and not the node we actually talk to.
	void connectToNode(std::pair<byteVec, byteVec>& ipAndToken, NODE_NAMES dst, PathManager* pm, NODE_NAMES overrideNodeFailure = NODE_NAMES::NO_NODE);
	std::pair<byteVec, byteVec> getNewNode();//the ip and the token
	//DOES NOT GARANTUE that the node connected to will be the one supplied. (if it is problematic, will switch)
	//overrideNodeFailure means if the path is invalid the msg will still be sent
	void establishConnWithGuard(PathManager* pm, byteVec ip = byteVec(), NODE_NAMES overrideNodeFailure = NODE_NAMES::NO_NODE);//ipAndToken of a node received form DS
	//THROWS Communication error if guard fails
	//DOES NOT GARANTUE that the node connected to will be the one supplied. (if it is problematic, will switch)
	//dst is the node to connect to.
	void establishConnWithNonGuard(NODE_NAMES dst, PathManager* pm,
		std::pair<byteVec, byteVec> ipAndToken = std::pair<byteVec, byteVec>(),
		NODE_NAMES overrideNodeFailure =NODE_NAMES::NO_NODE);
	//if the DSIPs parameter is given, then instead of using the db to get IPs of data servers, the funciton willl use the vector supplied.
	//IMPORTANT: cm mtx must be locked before calling the function!
	//it is done to prevent unwanted sending between getting a comm exceptoin and calling the function!
	void connectDS(std::vector<byteVec> DSIPs = std::vector<byteVec>());
	PathGenerator(std::vector<byteVec> DSIPs = std::vector<byteVec>());

	//sets relevant data to pm
	//dst is the node to key exchange with.
	//THROWS CommunicationException
	void performORKeyExchange(PathManager* pm, NODE_NAMES dst, NODE_NAMES overrideNodeFailure = NODE_NAMES::NO_NODE);
public:
	//will only report if there is an internet conn (returns true if reported sucessfully)
	bool reportNode(byteVec nodeIp);
	void reconnectGuardNode(PathManager* pm);
	void reconnectNonGuardNode(NODE_NAMES isMid, PathManager* pm);
	PathManager* generatePath();
	//THROWS noGuardsAvailException if no guards are availible.
	byteVec getGuardFromDS(std::vector<byteVec> guardIPs, std::vector<byteVec> DSIPs = std::vector<byteVec>());
	static PathGenerator& instance(std::vector<byteVec> DSIPs = std::vector<byteVec>());
};
