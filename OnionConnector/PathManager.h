﻿#pragma once
#include <iostream>
#include <future>
#include "CreateConn.h"
#include "Communicator.h"
#include "EndConnectionOutgoing.h"
#include "EndConnectionIngoing.h"
#include "ToSendMsg.h"
#include "EncryptionUtils.h"
#include <array>
#define SECS_TO_WAIT 10
#define SECS_TO_WAIT_QUICK 1
#define ID_LEN 8
#define END_CONN_FLAG_SHIFT 2
#define REGULAR_MSG_VALUE 0//except the final flag msg of course
#define NO_TIMEOUT -1
#define ONE_ACTIVE_THREAD 1
#define NUM_OF_THREADS 3
#define NUM_OF_NODES 3
class PathGenerator;
class PathManager;
class Communicator;

enum class NODE_NAMES;
enum class HEADERS;
enum class BITS_HEADER;
enum class MSG_TYPES;

enum class THREADS {
	ENSURE_GUARD_CONN_THREAD,
	RECV_THREAD
};
enum class NON_GUARD_NODE_FIXING_THREADS {
	MID_NODE,
	EXIT_NODE
};
typedef std::vector<byte> byteVec;
typedef std::unique_lock<std::mutex> unqLck;


typedef struct PMConnComm {
	PathManager* pm;
	std::queue<InterThreadMsg*> received;
	std::mutex receivedMtx;
	std::condition_variable receivedCv;
}PMConnComm;



class PathManager {
	friend class PMCFBridger;
	std::mutex _keysMtx;
	//array of 3 arrays representing the keys to each node
	std::array<std::array<byte, CryptoPP::AES::MAX_KEYLENGTH>, NUM_OF_NODES > _keys =
		std::array<std::array<byte, CryptoPP::AES::MAX_KEYLENGTH>, NUM_OF_NODES >();

	std::mutex _nodesDetailsMtx;
	byteVec _guardIp;

	byteVec _midIp;
	byteVec _midToken;

	byteVec _exitIp;
	byteVec _exitToken;

	bool _guardIsDown = false;
	std::mutex _guardIsDownMtx;
	std::condition_variable _guardIsDownCV;

	std::mutex _threadsMtx;
	std::vector<std::thread*> _threads = std::vector<std::thread*>({ nullptr, nullptr });
	std::atomic<bool> _toShutDownThreads = false;//whether to shut down threads
	
	std::condition_variable _nodeStatusesCv;
	std::mutex _nodeStatusesMtx;
	//false if failed
	std::array<bool, (byte)NODE_NAMES::EXIT_NODE + 1> _nodeStatuses = { true, true, true };

	std::mutex _nonGuardNodeFixingThreadsMtx;
	std::array<std::thread*, (byte)NON_GUARD_NODE_FIXING_THREADS::EXIT_NODE + 1> _nonGuardNodeFixingThreads = { nullptr, nullptr };//first is mid, sec is exit
	
	std::mutex _cmSendMtx;
	Communicator _cm;

	std::mutex _connsMtx;
	std::unordered_map<uint64_t, PMConnComm*> _conns;

	std::mutex _recvQueueMtx;
	std::condition_variable _recvQueueCv;
	std::queue<InterThreadMsg*> _recvQueue;

	std::mutex _directMsgsMtx;
	std::condition_variable _directMsgsCv;
	std::queue<UnprocessedMsg> _directMsgs;

	std::atomic<byte> _checkGuardMsgsCout = 0;

	std::mutex _createConnQueueMtx;
	std::queue<InterThreadMsg*> _createConnQueue;
	std::condition_variable _createConnCV;;

	std::atomic<bool> _isPrimary = true;
	//Msg must be a multiple of CryptoPP::AES::BLOCK_SIZE and not 0
	std::vector<byte> encrypt(std::vector<byte> cipherText, NODE_NAMES target);
	//Msg must be a multiple of CryptoPP::AES::BLOCK_SIZE and not 0
	std::vector<byte> decrypt(std::vector<byte> cipherText, NODE_NAMES origin);

	//responsible for forwarding messages to conn. ran by the init.
	void receiveThread();
	void handleNonGuardServerCrash(UnprocessedMsg msg);
	void handleInterThreadMsg(InterThreadMsg* msg);
	void ensureGuardNodeConnThread();
	//THROWS NodeCrashException
	void checkNodeCrash(NODE_NAMES fixingNode);
	//to force using new for creating this object.
	~PathManager();
	//will wait as long as there is no valid path.
	//waitForFix means that the funciton will wait even if the path is currently ok, untill a node will disconnect and then untill the path will be ok again.. 
	//the unique lck given MUST be a lock with nodeStatusesMtx;
	void waitForPathToBeOk(std::unique_lock<std::mutex>& lck, bool waitForFix = false);
	void cleanQueue(std::queue<InterThreadMsg*> queue);
public:	
	//will wait as long as there is no valid path.
	//waitForFix means that the funciton will wait even if the path is currently ok, untill a node will disconnect. 
	void waitForPathToBeOk(bool waitForFix = false);
	//Token MUST be given if this is mid or exit node. in the guard it is ignored.
	void setNodeDetails(NODE_NAMES node, byteVec ip, byteVec token = std::vector<byte>());
	//WILL RETURN SECOND EMPTY BYTEVEC FOR GUARD NODE
	//THROWS UninitiatedValue if node details weren't set!
	std::pair<byteVec, byteVec> getNodeDetails(NODE_NAMES node);
	//THIS FUNCTION WILL NOT CHANGE THE DATA REGARDING THE NODE.
	//CALL setNodeDetails after this fucntion to set the ip and token.
	//THIS FUNCTION WILL NOT EXCHANGE TOKENS WITH THE NODE. CALL PG's appropriate functions for that!!!
	//returns the ip connected to.
	byteVec connectToNewGuardNode(byteVec ip = byteVec());
	//MUST BE CALLED WITH THE _nodeStatusesMtx locked! Overwise undefiend behaviour!
	bool getIsPathOk();
	//responsible for called the send of cm.
	void sendConn(InterThreadMsg* msg);
	//THROWS CommunicationException!
	//IF NODE IS DOWN, will:
	//THROWS NodeCrashException untill it is ok again.
	//sending will reset the recvied msgs.
	//overridePathEnsurance menas when not to throw NodeCrashException.
	//NODE_NODE means always throw no matter which node has crashed.
	//EXIT_NODE means we repair the exit and the fact the exit crash shouldn't make the func throw.
	//MID_NODE means we repair the mid and the fact the mid crashed shouldn't make the func throw.
	//GUARD_NODE means we repair the guard and the funciton should never throw..
	void sendMsgDirectly(byteVec msg, MSG_TYPES type,
		NODE_NAMES dst = NODE_NAMES::GUARD_NODE, NODE_NAMES overridePathEnsurance = NODE_NAMES::NO_NODE);
	//returns one message at a time 
	//BLOCKS if no messages exist
	//THROWS NodeCrashException!
	//THROWS TimeOut!
	//overridePathEnsurance menas which node we are trying to fix, if trying to fix. this will override checks to ensure path is valid.
	//NODE_NODE means always throw no matter which node has crashed.
	//EXIT_NODE means we repair the exit and the fact the exit crash shouldn't make the func throw.
	//MID_NODE means we repair the mid and the fact the mid crashed shouldn't make the func throw.
	//GUARD_NODE means we repair the guard and the funciton should never throw.
	byteVec recvPathGeneration(int timeoutMilliSec = NO_TIMEOUT, NODE_NAMES overridePathEnsurance = NODE_NAMES::NO_NODE);
	void setIsPrimary(bool value);
	//THROWS CommunicationException!
	PathManager(byteVec ip);
	//IF GUARD IS DOWN, will block untill it is good again.
	//creates a conn in the rmeote. returns the id
	//THROWS TimeOut after SECS_TO_WAIT secs. 
	//THROWS NodeCrashException
	//THROWS ConnectionCreationError
	//THROWS CommunicationException
	//will not recv any existing msgs.
	uint64_t sendRecvConnCreationMsg(byteVec ipAndPort);
	//gets the id of the connection, returns the data relevant for the connection.
	PMConnComm* addConnection(uint64_t id);
	//for convinience a byteVec is received, however it must be CryptoPP::AES::MAXKEYLENGTH long!!
	void setKey(byteVec key, NODE_NAMES node);
};
