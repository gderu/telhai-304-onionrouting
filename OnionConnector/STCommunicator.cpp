#include "STCommunicator.h"



void STCommunicator::closeCommunication()
{
	WSACloseEvent(_readEvent);
	ICommunicator::closeCommunication();
}

void STCommunicator::connectToServer()
{
	ICommunicator::connectToServer();
	_readEvent = WSACreateEvent();
	WSAEventSelect(_connectSocket, _readEvent, FD_READ | FD_CLOSE);
}

void STCommunicator::setCommunicationDetails(std::vector<byte> ip)
{
	ICommunicator::setCommunicationDetails(ip, (unsigned short)DS_PORT);
}

void STCommunicator::sendToServer(std::vector<byte> toSend, byte flags)
{
	if (INVALID_SOCKET == _connectSocket)
	{
		throw (UninitiatedConnection());
	}
	const char* arrPtr;
	int iResult;
	std::vector<byte> sizeOfMsgBytes = Utilities::ShortIntTobigEndianBytes((short int)toSend.size());
	toSend.insert(toSend.begin(), sizeOfMsgBytes.begin(), sizeOfMsgBytes.end());
	arrPtr = (const char*)(toSend).data();
	iResult = send(_connectSocket, arrPtr, (int)toSend.size(), 0);
	if (iResult == SOCKET_ERROR) {
		closesocket(_connectSocket);
		WSACleanup();
		throw(CommunicationException("send failed with error: %d\n", WSAGetLastError()));
	}
}


std::vector<byte> STCommunicator::receiveFromServer(unsigned int timeoutMilli)
{
	if (INVALID_SOCKET == _connectSocket)
	{
		throw (UninitiatedConnection());
	}
	int bytesRead = 0;
	char buffer[BUFF_LEN] = { 0 };
	std::vector<byte> result;
	short int totalLength = 0;
	//finding length of message
	int rv = WSAWaitForMultipleEvents(1, &_readEvent, FALSE, timeoutMilli, TRUE);
	if (WSAResetEvent(_readEvent) == FALSE) {
		throw(CommunicationException("Communication error!", WSAGetLastError()));
	}
	if (WSA_WAIT_EVENT_0 == rv) {
		int bytesReadTotal = bytesRead = recv(_connectSocket, buffer, LEN_DESCRIPTION_LEN, 0);
		if (bytesRead == sizeof(unsigned short) / 2){
			bytesRead += recv(_connectSocket, buffer + 1, LEN_DESCRIPTION_LEN - 1, 0);//receiving the remaning byte which describes packet len.
		}
		else if (bytesRead < 0) {
			throw(CommunicationException("Communication error!", WSAGetLastError()));
		}
	}
	else {
		throw TimeOut();
	}
	totalLength = Utilities::bigEndianBytesToShortInt(std::vector<byte>(buffer, buffer + BUFF_LEN));
	int bytesReadTotal = 0;
	if (totalLength != 0) {

		while (true)
		{
			rv = WSAWaitForMultipleEvents(1, &_readEvent, FALSE, timeoutMilli, TRUE);
			if (WSAResetEvent(_readEvent) == FALSE) {
				throw(CommunicationException("Communication error!", WSAGetLastError()));
			}
			if (WSA_WAIT_EVENT_0 == rv) {
				bytesRead = recv(_connectSocket, buffer, BUFF_LEN > totalLength - bytesReadTotal ? totalLength - bytesReadTotal : BUFF_LEN, 0);
			}
			else {
				throw TimeOut();
			}
			bytesReadTotal += bytesRead;
			if (bytesRead > 0) {
				if (bytesReadTotal < totalLength)
				{
					result.insert(result.end(), buffer, buffer + bytesRead);
				}
				else
				{
					result.insert(result.end(), buffer, buffer + bytesRead);
					break;
				}
			}
			else {
				throw(CommunicationException("recv failed.", WSAGetLastError()));
			}
		}
	}
	return result;
}
