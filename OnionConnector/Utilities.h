#pragma once
#include <vector>
#include <string>
#include <atlbase.h>
#include <Netlistmgr.h>
#include <cmath>
#include "exceptions.cpp"
#define LEAST_SIGNIFICANT_BYTE_SHORT_INT 1
#define VALUES_IN_BYTE 256
#define BITS_FROM_SIGNIFIANT_TO_LEAST 7
typedef unsigned char byte;
class Utilities
{
public:
	//THROWS LackingBytes if less than 2 bytes were given!
	//will only refer to the first 2 bytes/
	static inline short int bigEndianBytesToShortInt(std::vector<byte> shortInt)
	{
		if (shortInt.size() < sizeof(short int)) {
			throw(LackingBytes());
		}
		else {
			return shortInt[0] *( 0xFF +1)+ shortInt[LEAST_SIGNIFICANT_BYTE_SHORT_INT];
		}
	}
	//THROWS LackingBytes if less than the length of T was given!
	//will only refer to the first sizeof(T) bytes/
	template <class T>
	static inline T bigEndianBytesToIntegerType(std::vector<byte> n)
	{
		if (n.size() < sizeof(T)) {
			throw(LackingBytes());
		}
		T toReturn = 0;
		for (int i = 0; i < sizeof(T); i++) {
			toReturn |= n[i] << ((sizeof(T) - i - 1) * 8);
		}
		return toReturn;
	}

	template <class T>
	static inline std::vector<byte> IntegerTypeTobigEndianBytes(T n)
	{
		std::vector<byte> res(sizeof(T));
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			res[i] = (byte)(n - (n >> 8 << 8));
			n = n >> 8;
		}
		return res;
	}

	static inline std::vector<byte> ShortIntTobigEndianBytes(short int shortInt)
	{
		std::vector<byte> res;
		res.resize(sizeof(short int));
		res[0] = (byte)shortInt / VALUES_IN_BYTE;
		res[LEAST_SIGNIFICANT_BYTE_SHORT_INT] = (byte)shortInt % VALUES_IN_BYTE;
		return res;
	}

	static std::vector<std::string> split(std::string s, std::string delimeter);


	static std::string join(std::vector<byte> vec, std::string delimeter);
	
	//byteToISolate starts from the lest significant (0) to the most significant (7)
	static byte isolateBit(byte aByte, byte byteToIsolate);
	
	//bitToZero starts from the lest significant (0) to the most significant (7)
	static byte zeroBit(byte aByte, byte bitToZero);

	static bool isIPV4InternetConnectionOk();
};

