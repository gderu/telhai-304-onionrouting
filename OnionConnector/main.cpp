#include "ConnectionFactory.h"
#include <iostream>
#include <vld.h>
#define CONNS_NUMBER 100

std::string makeLongMsg(int a, int b = 0) {
	std::string str;
	if (b == 0) {
		b = a;
		a = 0;
	}
	for (int i = a; i < b; i++) {
		str += std::to_string(i);
		if (i != b - 1) {
			str += " ";
		}
	}
	return str + "\n";
}

std::atomic<size_t> done;

struct passToCallback {
	std::string msg;
	std::atomic<size_t> numReceived;
};



void emptyCallback(void* data, ReceivedMsg msg, bool isEnd) {
	if (isEnd) {
		done++;
		std::cout << done << " GOT END MSG\n";
		return;
	}
	passToCallback* msgReceived = static_cast<passToCallback*>(data);
	msgReceived->msg += std::string((char*)msg.getMsg().data(), msg.getMsg().size());
	if (msgReceived->numReceived == 0 && msgReceived->msg == makeLongMsg(0, 600)) {
		msgReceived->msg = "";
		msgReceived->numReceived += 1;
	}
	else if (msgReceived->numReceived == 1 && msgReceived->msg == makeLongMsg(1200, 1800)) {
		msgReceived->msg = "";
		msgReceived->numReceived += 1;
	}

};

int main() {
	Sleep(100);
	for (int j = 0; j < 1000; j++) {
		std::cout << "hi";
		std::vector<Connection*> conns;
		std::vector<passToCallback> msgsReceived(CONNS_NUMBER);
		std::function<void(void*, ReceivedMsg, bool)> fnc = emptyCallback;
		ConnectionFactory& cf = ConnectionFactory::instance();
		for (unsigned int i = 0; i < CONNS_NUMBER; i++) {
			std::cout << "creatingConn " << i << "\n";
			conns.push_back(cf.createConnection("127.0.0.1", 80, fnc, &msgsReceived[i]));
		}

		for (size_t i = 0; i < conns.size(); i++) {
			while (msgsReceived[i].numReceived != 1) {
			}
			conns[i]->sendMsg(makeLongMsg(600, 1200));
			while (msgsReceived[i].numReceived != 2) {
			}
			conns[i]->sendMsg(makeLongMsg(1800, 2400));
		}
		std::cout << "DONE\n";
		while (done < CONNS_NUMBER) {
		}
		std::cout << "deletingConn\n";

		for (Connection* conn : conns) {
			delete conn;
		}
		conns.resize(0);
	}
	system("PAUSE");
	return 0;
}