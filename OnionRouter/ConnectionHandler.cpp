#include "ConnectionHandler.h"

SOCKET ConnectionHandler::initListenSocket()
{
    int iResult = 0;

    SOCKET listenSocket = INVALID_SOCKET;
    sockaddr_in service;

    
    listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);//initalizing the socket
    if (listenSocket == INVALID_SOCKET) {
        wprintf(L"socket function failed with error: %ld\n", WSAGetLastError());
        _endClass = true;
        return INVALID_SOCKET;
    }
    service.sin_family = AF_INET;
    InetPton(AF_INET, _T("0.0.0.0"), &service.sin_addr.s_addr);
    service.sin_port = htons(NODE_PORT);

    iResult = bind(listenSocket, (SOCKADDR*)&service, sizeof(service));//binding the socket to listen in an IP and port
    if (iResult == SOCKET_ERROR) {
        wprintf(L"bind function failed with error %d\n", WSAGetLastError());
        iResult = closesocket(listenSocket);
        if (iResult == SOCKET_ERROR)
            wprintf(L"closesocket function failed with error %d\n", WSAGetLastError());
        _endClass = true;
        return INVALID_SOCKET;
    }

    if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {//makes the socket start listening
        wprintf(L"listen function failed with error: %d\n", WSAGetLastError());
        iResult = closesocket(listenSocket);
        if (iResult == SOCKET_ERROR) {
            wprintf(L"closesocket function failed with error %d\n", WSAGetLastError());
        }
        _endClass = true;
        return INVALID_SOCKET;
    }
    return listenSocket;
}

SOCKET ConnectionHandler::initConnectSocket(std::vector<byte> IP, unsigned short port)
{
    SOCKET toReturn;
    int iResult;
    toReturn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (toReturn == INVALID_SOCKET) {
        throw CommunicationException("Communication exception!", WSAGetLastError());
    }
    //----------------------
    // The sockaddr_in structure specifies the address family,
    // IP address, and port of the server to be connected to.
    sockaddr_in service;
    service.sin_family = AF_INET;
    InetPton(AF_INET, (PCWSTR)Utilities::join(IP, ".").c_str(), &service.sin_addr.s_addr);
    service.sin_port = htons(port);

    //----------------------
    // Connect to server.
    iResult = connect(toReturn, (SOCKADDR*)&service, sizeof(service));
    if (iResult == SOCKET_ERROR) {
        iResult = closesocket(toReturn);
        throw CommunicationException("Communication exception!", WSAGetLastError());
    }
    return toReturn;
}

void ConnectionHandler::deallocSocket(SOCKET sock)
{
    sock = closesocket(sock);
    if (sock == SOCKET_ERROR) {
        wprintf(L"closesocket function failed with error %d hi\n", WSAGetLastError());
        _endClass = true;
        return;
    }

    _endClass = true;
}

void ConnectionHandler::listenThread()
{
    // Timeout parameter
    timeval tv = { 0 };
    std::cout << "HERE\n";

    tv.tv_usec = SELECT_SECOND / 10;//tenth of a second
    SOCKET listenSock = initListenSocket();
    if (listenSock == INVALID_SOCKET) {
        return;
    }
    std::cout << "HERE\n";

    while (!_endClass) {
        fd_set readfds;

        FD_ZERO(&readfds);
        // Set server socket to set
        FD_SET(listenSock, &readfds);

        int ret = select(0, &readfds, NULL, NULL, &tv);
        if (ret > 0) {
            // Accept incoming connection and add new socket to list
            SOCKET newSocket = accept(listenSock, NULL, NULL);
            if (newSocket != INVALID_SOCKET) {//if the accept succeeds
                std::unique_lock<std::mutex> newConnHandlersThreadsLck(_newConnHandlersThreadsMtx);
                _newConnHandlersThreads.push_back(std::thread(&ConnectionHandler::handleNewConnThread, this, newSocket));//initalizes a new thread to take care of the connection
            }
            else {//if the accept failed, the socket isn't working, so just continue accepting
                continue;
            }
        }
        else if (ret < 0) {
            break;
        }
    }
    std::cout << "OUT " << WSAGetLastError() << std::endl;
    deallocSocket(listenSock);
    return;
}

void ConnectionHandler::handleNewConnThread(SOCKET prevSocket)
{
    std::vector<byte> receivedMsg, toSend;
    std::array<byte, 2> flags = {};
    ConnectionLeft* prev = new ConnectionLeft(prevSocket);//makes prevSocket into a ConnectionLeft, to more easily receive and send messages
    if (!prev->recvOne(receivedMsg, flags)) {//gets the initial message, which tells us what kind of node this will be, as well as sends the first of part of the connection token
        delete prev;
        return;
    }
    if (flags[MSG_TYPE] != (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG) {//if the msg isn't a path generation msg, stop
        delete prev;
        return;
    }
    std::vector<byte> token;
    bool didReconnectNode;
    if (prev->getNodeName() != NODE_NAMES::GUARD_NODE) {//if its a guard, a token isn't needed, so doesn't check for token if it is a guard
        try {
            didReconnectNode = checkNeedReconnect(prev, receivedMsg, token);
        }
        catch (TimeOut) {
            return;
        }
    }

    token.insert(token.begin(), 1);

    if (prev->getNodeName() == NODE_NAMES::GUARD_NODE || !didReconnectNode && prev->getNodeName() == NODE_NAMES::MID_NODE) {//the token wasn't already in the NWay map as well as in the TwoWay map, so couldn't reconnect to it
        if (!handleGuardMid(prev, token, flags, receivedMsg)) {
            return;
        }
    }
    else if (!didReconnectNode){
        if (!prev->sendMsg(token, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG)) {
            delete prev;
            return;
        }
        std::unique_lock<std::mutex> currentExitNodesLck(_currentExitNodesMtx);//adding a new exit node
        _currentExitNodes.insert(std::pair<std::vector<byte>, std::unique_ptr<NWay>>(token, new NWay(prev, std::bind(&ConnectionHandler::notifyEndClass, this, token, NODE_NAMES::EXIT_NODE))));
    }
}

void ConnectionHandler::removeUnusedNodesThread()
{
    std::unique_lock<std::mutex> toRemoveLck(_toRemoveMtx);
    while (!_endClass) {
        _toRemoveCV.wait(toRemoveLck, [this] { return _endClass || _toRemoveQueue.size() > 0; });//wait while _endClass isn't on and there isn't anything new to remove

        if (_toRemoveQueue.empty()) {//if it stopped because of _endClass
            break;
        }
        while(!_toRemoveQueue.empty()){//while there are still objects to remove
            std::pair<std::vector<byte>, NODE_NAMES> temp = _toRemoveQueue.front();
            _toRemoveQueue.pop();
            std::vector<byte> token = temp.first;
            NODE_NAMES nodeType = temp.second;
            //erasing the nodesthat appear in _toRemoveQueue
            if (nodeType == NODE_NAMES::EXIT_NODE) {
                std::unique_lock<std::mutex> currentExitNodeLck(_currentExitNodesMtx);
                auto it = _currentExitNodes.find(token);
                if (it != _currentExitNodes.end()) {
                    _currentExitNodes.erase(it);
                    std::cout << "Erased exit node " << _currentExitNodes.size() << std::endl;
                }
            }
            else if (nodeType == NODE_NAMES::MID_NODE) {
                std::unique_lock<std::mutex> currentMidNodesLck(_currentMidNodesMtx);
                auto it = _currentMidNodes.find(token);
                if (it != _currentMidNodes.end()) {
                    _currentMidNodes.erase(it);
                    std::cout << std::string("Erased mid node ") + std::to_string(_currentMidNodes.size()) << std::endl;
                }
            }
            else {
                std::unique_lock<std::mutex> currentGuardNodesLck(_currentGuardNodesMtx);
                auto it = _currentGuardNodes.find(token);
                if (it != _currentGuardNodes.end()) {
                    _currentGuardNodes.erase(it);
                    std::cout << std::string("Erased guard node ") + std::to_string(_currentGuardNodes.size()) + "\n";
                }
            }
        }
    }
}

bool ConnectionHandler::handleGuardMid(ConnectionLeft* prev, std::vector<byte> token, std::array<byte, 2> flags, std::vector<byte> receivedMsg)
{
    if (!prev->sendMsg(token, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG)) {//sends a msg with the remainder of the token, telling OC that everything went smoothly
        delete prev;
        return false;
    }
    if (!prev->recvOne(receivedMsg, flags)) {
        std::cout << "fails recv" << std::endl;
        delete prev;
        return false;
    }

    if (flags[MSG_TYPE] != (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG) {
        std::cout << "not path gen msg" << std::endl;
        delete prev;
        return false;
    }
    try {
        if (prev->getNodeName() == NODE_NAMES::MID_NODE) {
            std::unique_lock<std::mutex> currentMidNodesLck(_currentMidNodesMtx);//initializing a new TwoWay mid, and adding it to the map
            _currentMidNodes.insert(std::pair<std::vector<byte>, std::unique_ptr<TwoWay>>(token, new TwoWay(prev, std::vector<byte>(receivedMsg.begin(), receivedMsg.begin() + IP_LEN), std::bind(&ConnectionHandler::notifyEndClass, this, token, NODE_NAMES::MID_NODE))));
        }
        else {
            token = Utilities::IntegerTypeTobigEndianBytes<uint64_t>(_guardNodeToken++);//need a pointer to each guard, so having a token for each guard node
            std::unique_lock<std::mutex> currentGuardNodesLck(_currentGuardNodesMtx);
            _currentGuardNodes.insert(std::pair<std::vector<byte>, std::unique_ptr<TwoWay>>(token, new TwoWay(prev, std::vector<byte>(receivedMsg.begin(), receivedMsg.begin() + IP_LEN), std::bind(&ConnectionHandler::notifyEndClass, this, token, NODE_NAMES::GUARD_NODE))));
        }
    }
    catch (CommunicationException) {
        prev->sendMsg({ 0 }, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG);//if failed, send a failed msg and delete the msg
        delete prev;
        return false;
    }
    return true;
}

bool ConnectionHandler::checkNeedReconnect(ConnectionLeft* prev, std::vector<byte> receivedMsg, std::vector<byte>& token)
{
    try {
        token = getToken(std::vector<byte>(receivedMsg.begin(), receivedMsg.begin() + TOKEN_FIRST_HALF));//gets the corresponding token to the part sent to us
    }
    catch (TimeOut) {//if didn't receive the token after a certain amount of time
        prev->sendMsg({ 0 }, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG);//sending a msg with 0 first, meaning that the process failed
        delete prev;
        throw TimeOut();
    }
    return reconnectNode(prev, token);//tried reconnecting the node to the correct token
}

bool ConnectionHandler::reconnectNode(ConnectionLeft* prev, std::vector<byte> token)
{
    std::vector<std::vector<byte>> halfTokenExitNodes;
    std::vector<std::vector<byte>> halfTokenMidNodes;
    std::unique_lock<std::mutex> currentExitNodesLck(_currentExitNodesMtx), currentMidNodesLck(_currentMidNodesMtx);

    std::transform(_currentMidNodes.begin(), _currentMidNodes.end(), std::back_inserter(halfTokenMidNodes), [](std::pair<const std::vector<byte>, std::unique_ptr<TwoWay>>& pair) { return std::vector<byte>(pair.first.begin(), pair.first.begin() + TOKEN_FIRST_HALF); });
    std::transform(_currentExitNodes.begin(), _currentExitNodes.end(), std::back_inserter(halfTokenExitNodes), [](std::pair<const std::vector<byte>, std::unique_ptr<NWay>>& pair) { return std::vector<byte>(pair.first.begin(), pair.first.begin() + TOKEN_FIRST_HALF); });

    auto itExitNodes = std::find(halfTokenExitNodes.begin(), halfTokenExitNodes.end(), token);
    auto itMidNodes = std::find(halfTokenMidNodes.begin(), halfTokenMidNodes.end(), token);
    if (itExitNodes == halfTokenExitNodes.end() && itMidNodes == halfTokenMidNodes.end()) {//the token is not found
        return false;
    }
    else {
        prev->sendMsg(std::vector<byte>{(byte)NODE_MSGS::RECONNECT_TO_NODE, true}, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG);
    }
    if (itExitNodes != halfTokenExitNodes.end()) {//the token is for an NWay
        _currentExitNodes.at(*itExitNodes)->switchSocket(prev->getSocket());
    }
    else {//the token is for a TwoWay
        _currentMidNodes.at(*itExitNodes)->switchSocket(prev->getSocket());
    }
    prev->shutdownClass(false);//shutting down ConnectionLeft without actually closing the socket, so that the other node that the socket was just passed to will still be able to use it
    delete prev;
    return true;
}

void ConnectionHandler::notifyEndClass(std::vector<byte> token, NODE_NAMES nodeType)
{
    std::unique_lock<std::mutex> toRemoveLck(_toRemoveMtx);
    _toRemoveQueue.push({ token, nodeType });//adding the token that is to be removed to the queue
    toRemoveLck.unlock();

    _toRemoveCV.notify_one();
}

std::vector<byte> ConnectionHandler::getToken(std::vector<byte> firstPart)//Soteria
{
    std::unique_lock<std::mutex> tokensLck(_tokensMtx);
    uint64_t tokenID = Utilities::bigEndianBytesToIntegerType<uint64_t>(firstPart);
    uint64_t toReturn;
    try {
        toReturn = _tokens.at(tokenID);
    }
    catch (std::out_of_range) {//if the tokenID isn't in the map
        if (!_tokensCV.wait_for(tokensLck, std::chrono::seconds(5), [this, tokenID] { return _tokens.find(tokenID) != _tokens.end(); })) {
            throw TimeOut();
        }
        else {
            toReturn = _tokens.at(tokenID);
        }
    }
    return Utilities::IntegerTypeTobigEndianBytes<uint64_t>(toReturn);
}

ConnectionHandler::ConnectionHandler(): _dsConn(_tokens, _tokensMtx, _tokensCV)
{
    WSADATA wsaData;
    // Initialize Winsock
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    _listenThread = std::thread(&ConnectionHandler::listenThread, this);
    _removeUnusedNodesThread = std::thread(&ConnectionHandler::removeUnusedNodesThread, this);
}

ConnectionHandler::~ConnectionHandler()
{
    _endClass = true;
    _listenThread.join();
    _toRemoveCV.notify_one();
    _removeUnusedNodesThread.join();
    std::unique_lock<std::mutex> newConnHandlersThreadsLck(_newConnHandlersThreadsMtx);
    std::for_each(_newConnHandlersThreads.begin(), _newConnHandlersThreads.end(), [](std::thread& thr) { thr.join(); });//erasing each newConnHandlersThread
    newConnHandlersThreadsLck.unlock();
    WSACleanup();

}
