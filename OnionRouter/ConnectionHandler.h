#pragma once
#include <functional>
#include "NWay.h"
#include "TwoWay.h"
#include "DSConn.h"
#define SELECT_SECOND 1000000
#define NODE_PORT 8012
#define IP_LEN 4
#define BYTE_SIZE 1
#define TOKEN_FIRST_HALF 8
#define TOKEN_SECOND_HALF 8

enum class NODE_MSGS {
	GIVE_TOKEN_AND_CONNECT,
	RECONNECT_TO_NODE
};


class ConnectionHandler
{
	std::vector<std::thread> _newConnHandlersThreads;
	std::map<std::vector<byte>, std::unique_ptr<TwoWay>> _currentGuardNodes;
	std::map<std::vector<byte>, std::unique_ptr<TwoWay>> _currentMidNodes;
	std::map<std::vector<byte>, std::unique_ptr<NWay>> _currentExitNodes;
	std::thread _listenThread, _removeUnusedNodesThread;
	std::atomic<bool> _endClass = false;
	std::mutex _currentExitNodesMtx, _currentMidNodesMtx, _currentGuardNodesMtx, _newConnHandlersThreadsMtx, _tokensMtx, _toRemoveMtx;
	std::unordered_map<uint64_t, uint64_t> _tokens;
	std::condition_variable _tokensCV, _toRemoveCV;
	DSConn _dsConn;
	std::atomic<uint64_t> _guardNodeToken = 0;
	std::queue<std::pair<std::vector<byte>, NODE_NAMES>> _toRemoveQueue;

	//initializes a new listening socket
	SOCKET initListenSocket();
	//initalizes a connection socket
	SOCKET initConnectSocket(std::vector<byte> IP, unsigned short port);
	//deallocates a socket
	void deallocSocket(SOCKET sock);

	//listents for new connections and passes them to handleNewConnThread
	void listenThread();
	//handles a new connection, and initializes the node that it will be. Adds that node to the node maps, then quits
	void handleNewConnThread(SOCKET prev);
	//thread that constantly runs and removes dead nodes from the node maps
	void removeUnusedNodesThread();

	//handles the socket if it a guard or a mid. Returns true if completed successfully, false if error occured
	bool handleGuardMid(ConnectionLeft* prev, std::vector<byte> token, std::array<byte, 2> flags, std::vector<byte> receivedMsg);

	//checks if the token needs to reconnect, and if it does reconnects. Returns false if didn't reconnect and true if reconnected
	//throws TimeOut
	bool checkNeedReconnect(ConnectionLeft* prev, std::vector<byte> receivedMsg, std::vector<byte>& token);

	//reconnects a node that has been disconnected. The input prev is the replacement ConnectionLeft, that is to be replaced in the class with the specified token.
	//returns true if found a node to replace, false if not
	bool reconnectNode(ConnectionLeft *prev, std::vector<byte> token);
	//is called when a node finishes running. Gets the node's token, and its type (guard, mid, or exit)
	void notifyEndClass(std::vector<byte> token, NODE_NAMES nodeType);
	//gets the second half of the token. If it isn't in the CV yet, waits 5 seconds and if it doesn't receive anything new throws TimeOut.
	std::vector<byte> getToken(std::vector<byte> firstPart);
public:
	ConnectionHandler();
	~ConnectionHandler();
};

