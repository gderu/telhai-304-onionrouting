#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>
#include <string>
#include <queue>
#include <mutex>
#include <algorithm>
#include <condition_variable>
#include <map>
#include <thread>
#include <ctime>
#include <atomic>
#include <array>
#include <iostream>
#include "Utilities.h"
#include "EncryptionUtils.h"
#define MSG_LEN 512
#define HEADER_SIZE 2
#define DATA_SIZE (512 - HEADER_SIZE)
#define MSG_ID_LEN 2
#define SECOND 1000
#define TEN_SECONDS 10
#define FIRST_HEADER_LEN 4
#define FOLLOWING_HEADER_LEN 1
#define MSG_TYPE 1
#define OTHER_FLAGS 0
#define NODE_NUM_LEN 2
#define SIZE_LEN 2
#define WAIT_IF_MID 20
#define WAIT_IF_EXIT 60
#define WAIT_FOR_CHECK_MSG 5

enum class MSG_TYPE_MEANING {
	OUTSIZE_MSG,
	CHECK_NEXT,
	CONNECTION_CLOSURE_MSG,
	PATH_GENERATION_MSG,
	CONN_CREATION_INITIAL,
	CONN_CREATION_CONFIRMATION,
	NODE_CRASH_MSG,
	PATH_CLOSURE_MSG,
	KEY_EXCHANGE_MSG
};

enum class FLAGS {
	LAST_FRAGMENT_BIT,
	IS_GUARD,
	IS_MID
};

#ifndef NODE_NAMES_DEFINED
#define NODE_NAMES_DEFINED
enum class NODE_NAMES {
	GUARD_NODE = 0,
	MID_NODE,
	EXIT_NODE,
	NO_NODE
};
#endif

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

typedef unsigned char byte;

class ConnectionLeft
{
	std::atomic<SOCKET> _connectSocket;
	std::queue<std::vector<byte>> _dataReceived;
	std::mutex _dataReceivedMtx, _sendMtx, _endClassMtx, _connectionEndedMtx, _connectionBrokenMtx, _finishShutdownMtx, _msgReceivedMtx;
	std::condition_variable _dataReceivedCV, _endClassCV, _connectionBrokenCV, _msgReceivedCV, _connectionEndedCV;
	std::atomic<bool> _endClass = false, _connectionEnded = false, _connectionBroken = false, _msgReceived = false;
	std::thread *_recvMsgsThread, *_timeoutMsgThread;
	std::atomic<WSAEVENT> _readEvent;
	NODE_NAMES _nodeName;
	std::vector<byte> _msg, _key;

	bool keyExchange();//does the key exchange once called, and stores result in _key. Returns false if either send or recv failed
	std::vector<byte> encrypt(std::vector<byte> msg);
	std::vector<byte> decrypt(std::vector<byte> cipherText);

	bool safeRecv(SOCKET s, char* buffer, size_t len);//THROWS CommunicationException. Performs recv safely. Returns true if success, false if connection closed
	void waitInternetIssue();//waits while there is an internet issue. Finishes running when connected to the internet.
	void recvMsgsThread();//thread that performs recv continuously, until the connection is closed
	void timeoutMsgThread();//thread that checks for timeout
	bool sendSingleMsg(std::vector<byte> toSend, byte msgType = 0, NODE_NAMES origin = NODE_NAMES::NO_NODE, bool isLast = false, int16_t lengthLast = 0);//Sends a single 512 byte long msg. Returns true if succesfull, false if the send failed and OC doesn't reroute in time. If lengthLast is -1, its not the first message
	std::vector<byte> getData(std::vector<byte> msg, NODE_NAMES target);
	void shutdownConnection();
public:
	ConnectionLeft() : _connectSocket(0), _readEvent(0), _nodeName(NODE_NAMES::GUARD_NODE), _recvMsgsThread(nullptr), _timeoutMsgThread(nullptr) {};
	ConnectionLeft(SOCKET connectSocket);//Initializes ConnectionLeft. Gets the socket on which the connection will be operating
	~ConnectionLeft();//Stops all threads then shuts down

	void operator=(ConnectionLeft&& other) noexcept;
	bool operator==(const SOCKET& otherSocket) const;
	bool sendMsg(std::vector<byte> toSend, byte msgType, NODE_NAMES origin = NODE_NAMES::NO_NODE, int16_t length = 0);//sends a msg on the socket
	bool recvOne(std::vector<byte>& recvMsg, std::array<byte, 2>& flags, int16_t& length, std::chrono::milliseconds waitTime = std::chrono::milliseconds(0));//blocking, gets a single message and puts it in recvMsg. Will only block for waitTime.
	bool recvOne(std::vector<byte>& recvMsg, std::array<byte, 2>& flags, std::chrono::milliseconds waitTime = std::chrono::milliseconds(0));//blocking, gets a single message and puts it in recvMsg. Will only block for waitTime.
	void shutdownClass(bool closeConnection = true, bool timeoutCalled = false, bool recvMsgCalled = false);//shuts down the entire class and signals all remaining threads to close
	void newSocket(SOCKET s);
	SOCKET getSocket();
	void setNodeName(NODE_NAMES nodeName);
	NODE_NAMES getNodeName();
};