#include "ConnectionRight.h"

ConnectionRight::ConnectionRight(byteVec ip, int temp): _temp(temp), _cm(_recvQueueMtx, _recvQueueCv, _recvQueue)
{
	_cm.setCommunicationDetails(ip);
	_cm.connectToServer();
	std::lock_guard<std::mutex> lck(_threadsMtx);
	_threads[(byte)THREADS::RECV_THREAD] = new std::thread(&ConnectionRight::receiveThread, this);
	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD] = new std::thread(&ConnectionRight::ensureGuardNodeConnThread, this);
}

void ConnectionRight::send(byteVec msg, std::array<byte, 2> flags, int16_t length)
{
	size_t len = msg.size();
	if (_hasNodeCrashed) {
		throw NodeCrashException();
	}
	if (_toShutDownThreads) {
		throw ShutdownException();
	}
	int toAddToMsg = msg.size();
	if (toAddToMsg <= MSG_LEN - HEADERS_LEN_START) {
		toAddToMsg = MSG_LEN - HEADERS_LEN_START - toAddToMsg;
	}
	else {
		toAddToMsg -= (MSG_LEN - HEADERS_LEN_START);
		toAddToMsg %= (MSG_LEN - HEADERS_LEN_LATE);
		toAddToMsg = MSG_LEN - HEADERS_LEN_LATE - toAddToMsg;
	}


	std::vector<byte> lastMsgLen = Utilities::IntegerTypeTobigEndianBytes<int16_t>(length);
	msg.resize(msg.size() + toAddToMsg);//converting msg to correct size
	msg.insert(msg.begin(), flags.begin(), flags.end());
	msg.insert(msg.begin() + flags.size(), lastMsgLen.begin(), lastMsgLen.end());

	if (msg.size() <= MSG_LEN) {
		msg[(int)HEADERS::BITS_HEADER] = Utilities::setBit(msg[(int)HEADERS::BITS_HEADER], (byte)BITS_HEADER::FINAL_FRAGMENT_FLAG, true);
	}
	else {
		size_t i = MSG_LEN;
		while (msg.size() > i + MSG_LEN) {
			msg.insert(msg.begin() + i, msg[(int)HEADERS::BITS_HEADER]);
			i += MSG_LEN;
		}
		byte bitsHeader = Utilities::setBit(msg[(int)HEADERS::BITS_HEADER], (byte)BITS_HEADER::FINAL_FRAGMENT_FLAG, true);
		msg.insert(msg.begin() + i, bitsHeader);
	}
	if (msg.size() > 5000) {
		int i = 0;
	}
	std::lock_guard<std::mutex> cmSendLck(_cmSendMtx);
	try {
		_cm.sendToServer(msg);
	}
	catch (CommunicationException) {//thrown if shutdown occured and the socket was closed. (may also occur if node disconnected, hanlding is identical)
		throw ShutdownException();
	}
}

void ConnectionRight::receiveThread()
{
	std::unique_lock<std::mutex> recvToForwardLck(_recvToForwardMtx, std::defer_lock);
	std::unique_lock<std::mutex> recvQueueLck(_recvQueueMtx, std::defer_lock);
	InterThreadMsg* tempMsg;
	byteVec tempVec;
	while (true) {
		recvQueueLck.lock();
		if (_recvQueue.size() == 0) {
			_recvQueueCv.wait(recvQueueLck, [this]() { return _recvQueue.size() > 0; });
		}
		if (_toShutDownThreads) {//noitfied by shutdown
			break;
		}
		tempMsg = _recvQueue.front();
		_recvQueue.pop();
		recvQueueLck.unlock();
		if (tempMsg->getType() == std::string("InterThreadCommunicationException")) {
			_hasNodeCrashed = true;
			_toShutDownThreads = true; // we know that the next node is destoryed.
			delete tempMsg;
			break;
		}
		else if (tempMsg->getType() == std::string("UnprocessedMsg") && ((UnprocessedMsg*)tempMsg)->getFlag() == MSG_TYPES::CHECK_NEXT) {
			//a check next node msg
			_checkNextNodeMsgsCount++;
			delete tempMsg;
		}
		else {
			recvToForwardLck.lock();
			UnprocessedMsg temp = *(UnprocessedMsg*)tempMsg;
			_recvToForward.push(temp);
			_recvToForwardCV.notify_all();
			recvToForwardLck.unlock();
			delete tempMsg;
		}
	}	
}


UnprocessedMsg ConnectionRight::recv(int timeoutMilliSec)
{
	if (_hasNodeCrashed) {
		throw NodeCrashException();
	}
	if (_toShutDownThreads) {
		throw ShutdownException();
	}
	unqLck recvToForwardLck(_recvToForwardMtx);
	if (_recvToForward.empty()) {
		if (timeoutMilliSec < 0) {
			_recvToForwardCV.wait(recvToForwardLck, [this]() { return _toShutDownThreads || _recvToForward.size() > 0; });
			if (_toShutDownThreads) {
				throw ShutdownException();
			}
		}
		else {
			_recvToForwardCV.wait_for(recvToForwardLck, std::chrono::milliseconds(timeoutMilliSec), [this]() { return _toShutDownThreads || _recvToForward.size() > 0; });
			if (_recvToForward.empty()) {
				if (_toShutDownThreads) {
					throw ShutdownException();
				}
				else {
					throw TimeOut();
				}
			}
		}
	}
	UnprocessedMsg msgFromComm = _recvToForward.front();
	_recvToForward.pop();
	std::vector<byte> msgData = msgFromComm.getMsg(), cleanMsg;
	if (msgData.size() > MSG_LEN - HEADERS_LEN_START) {
		cleanMsg = std::vector<byte>(msgData.begin() + HEADERS_LEN_START, msgData.begin() + MSG_LEN);
		size_t i = MSG_LEN;
		while (msgData.size() > i + MSG_LEN) {//getting the data from the message
			cleanMsg.insert(cleanMsg.end(), msgData.begin() + i + 1, msgData.begin() + i + MSG_LEN);
			i += MSG_LEN;
		}
		cleanMsg.insert(cleanMsg.end(), msgData.begin() + i + 1, msgData.end());
	}
	else {
		cleanMsg = std::vector<byte>(msgData.begin() + HEADERS_LEN_START, msgData.end());
	}
	UnprocessedMsg toReturn = { msgFromComm.getOrigin(), msgFromComm.getFlag(), cleanMsg, msgFromComm.getLength()};
	return toReturn;
}


void ConnectionRight::ensureGuardNodeConnThread()
{
	std::unique_lock<std::mutex> ensureNodeConnLck(_ensureNodeConnMtx);
	byteVec vec;
	std::unique_lock<std::mutex> cmSendLck(_cmSendMtx, std::defer_lock);
	bool isThereConn = false;
	byte flagsByte = TRUE;//last frag
	flagsByte += TRUE<< (byte)BITS_HEADER::IS_GUARD;//ask Ori. Does the message allways seem like it is sent by the guard?
	vec.push_back(flagsByte);
	vec.push_back((byte)MSG_TYPES::CHECK_NEXT);
	vec.push_back(0);
	vec.push_back(HEADERS_LEN_START);
	vec.resize(MSG_LEN);
	while (!_toShutDownThreads) {
		_checkNextNodeMsgsCount = 0;
		try {
			cmSendLck.lock();
			_cm.sendToServer(vec);
			cmSendLck.unlock();
		}
		catch (CommunicationException) {
			std::cout << std::string("commException") + std::to_string(_temp) + "\n";
			_toShutDownThreads = true;
			break;
		}
		if (_ensureNodeConnCV.wait_for(ensureNodeConnLck, std::chrono::milliseconds(SEC_IN_MILLI), [this]() { return _ensureNodeConnBool; })) {
			_ensureNodeConnBool = false;
			std::cout << std::string("no_timeout") + std::to_string(_temp) + "\n";
			break;
		}

		if (_checkNextNodeMsgsCount == 0) {
			_toShutDownThreads = true;
		}
	}
}

void ConnectionRight::shutdownClass()
{
	_toShutDownThreads = true;
	_recvQueueCv.notify_all();
	_recvToForwardCV.notify_all();
	_recvQueueCv.notify_all();
	std::lock_guard<std::mutex> threadsLck(_threadsMtx);
	_threads[(byte)THREADS::RECV_THREAD]->join();
	delete _threads[(byte)THREADS::RECV_THREAD];
	_threads[(byte)THREADS::RECV_THREAD] = nullptr;
	std::unique_lock<std::mutex> ensureNodeConnLck(_ensureNodeConnMtx);
	_ensureNodeConnBool = true;
	_ensureNodeConnCV.notify_all();
	ensureNodeConnLck.unlock();
	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD]->join();
	delete _threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD];
	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD] = nullptr;
	_cm.closeCommunication();
	InterThreadMsg* toDelete;
	std::unique_lock<std::mutex> outQueueLck(_recvQueueMtx);
	while (_recvQueue.size() != 0) {
		toDelete = _recvQueue.front();
		delete toDelete;
		_recvQueue.pop();
	}
}
