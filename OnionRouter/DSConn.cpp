#include "DSConn.h"


byteVec DSConn::getNextDS()
{
	byteVec ip;
	std::string str;
	byte count = 0;
	std::getline(_inputFile, str);
	if (_inputFile.eof()) {
		_inputFile.seekg(std::streampos(0));
		std::getline(_inputFile, str);
	}
	auto temp =  Utilities::split(str, ".");
	std::transform(temp.begin(), temp.end(), std::back_inserter(ip),
		[](const std::string& str) { return std::stoi(str); });
	return ip;
}

DSConn::DSConn(std::unordered_map<uint64_t, uint64_t>& tokens, std::mutex& tokensMtx, std::condition_variable& tokensCv)
	:_tokens(tokens), _tokensMtx(tokensMtx), _tokensCv(tokensCv)
{
	char str[IP_STR_LEN + 1] = { 0 };
	unsigned int i = 0;
	srand(time(NULL));
	byte randomInc = rand() % DS_AMOUNT;
	for (; i < randomInc; i++) {
		_inputFile.getline(str,IP_STR_LEN);
	}
	while (true) {
		try {
			_cm.setCommunicationDetails(getNextDS());
			_cm.connectToServer();
			break;
		}
		catch (CommunicationException e) {
			std::cout << e.getErrorMsg() << e.getErrorCode() << std::endl;
		}
	}
	makeInitialCommunicationAndKeyExchange();
	_recvThread = new std::thread(&DSConn::recvThread, this);
}

void DSConn::makeInitialCommunicationAndKeyExchange()
{
	byteVec response;
	byteVec msg;
	byteVec publicKey;
	byteVec dhKey;
	byteVec privateKey = EncryptionUtils::generateRand(PRIVATE_EXPONENT_LEN);
	msg = EncryptionUtils::power2(privateKey.data());//public key
	msg.push_back((byte)DS_MSGS::NODE_ALIVE);
	response = sendRecvDS(msg);
	publicKey.assign(response.begin(), response.begin() + DH_KEY_LEN);
	dhKey = EncryptionUtils::power(publicKey.data(), privateKey.data());
	_key = EncryptionUtils::generateKey(dhKey.data());
}


DSConn::~DSConn()
{
	_shutdown = true;
	_cm.closeCommunication();//will automatically exit the receiving funciton recvThread is using.
	_recvThread->join();
	delete _recvThread;
}



void DSConn::recvThread()
{
	byteVec received;
	byteVec receivedEncrypted;
	byteVec msgWithoutIv;
	std::unique_lock<std::mutex> lck(_tokensMtx, std::defer_lock);
	while (true) {
		try {
			receivedEncrypted = _cm.receiveFromServer();
			msgWithoutIv = byteVec(receivedEncrypted.begin() + EncryptionUtils::getIvLen(), receivedEncrypted.end());
			received = EncryptionUtils::decrypt(
				msgWithoutIv,
				_key.data(),
				receivedEncrypted.data());
		}
		catch (CommunicationException) {
			//its either that the ds crashed or that we closed the conn.
			if (_shutdown) {
				break;
			}
			else {
				reconnectDS();
				continue;
			}
		}
		lck.lock();
		uint64_t first = Utilities::bigEndianBytesToIntegerType<uint64_t>(std::vector<byte>(received.begin(), received.begin() + TOKEN_LEN / 2));
		uint64_t second = Utilities::bigEndianBytesToIntegerType<uint64_t>(std::vector<byte>(received.begin() + TOKEN_LEN / 2, received.end()));
		_tokens[first] = second;
		_tokensCv.notify_all();
		lck.unlock();
	}
}
byteVec DSConn::sendRecvDS(byteVec msg)
{
	while (true) {
		try {
			_cm.sendToServer(msg);
			return _cm.receiveFromServer(SEC_IN_MILLI * 5);
			break;
		}
		catch (CommunicationException) {
			reconnectDS();
		}
		catch (TimeOut) {
			reconnectDS();
		}
	}
}

void DSConn::reconnectDS()
{
	bool success = false;
	unsigned int i = 0;
	byteVec DSIP = getNextDS();
	_cm.closeCommunication();
	while (!success) {
		try {
			_cm.setCommunicationDetails(DSIP);
			_cm.connectToServer();			
			success = true;
		}
		catch (CommunicationException) {
			if (Utilities::isIPV4InternetConnectionOk()) {
				DSIP = getNextDS();
			}
		}
	}
}
