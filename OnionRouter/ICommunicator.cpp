#include "ICommunicator.h"

void ICommunicator::setCommunicationDetails(std::vector<byte> ip, unsigned short port)
{
	_port = port;
	_ipAddr = ip;
}

void ICommunicator::closeCommunication()
{
	shutdown(_connectSocket, SD_SEND);
	char buf[100];
	while (recv(_connectSocket, buf, 100, 0) > 0) {
	}
	closesocket(_connectSocket);
	WSACleanup();
	_connectSocket = INVALID_SOCKET;
}

void ICommunicator::connectToServer()
{
	if (_port == PORT_UNSET || _ipAddr.size() == 0) {
		throw UninitiatedValue();
	}
	WSADATA wsaData;
	struct addrinfo* result = NULL,
		* ptr = NULL,
		hints;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		throw(CommunicationException("Winsock failed to initialize", iResult));
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(Utilities::join(_ipAddr, std::string(".")).c_str(), std::to_string(_port).c_str(), &hints, &result);
	if (iResult != 0) {
		WSACleanup();
		throw(CommunicationException("getaddrinfo failed ", iResult));
	}
	
	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != nullptr; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		_connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (_connectSocket == INVALID_SOCKET) {
			WSACleanup();
			throw(CommunicationException("getaddrinfo failed ", WSAGetLastError()));
		}

		// Connect to server.
		iResult = connect(_connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			std::cout << WSAGetLastError() << std::endl;
			closesocket(_connectSocket);
			_connectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (_connectSocket == INVALID_SOCKET) {
		WSACleanup();
		throw(CommunicationException("Unable to connect to server", iResult));
	}
}
