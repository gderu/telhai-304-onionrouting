#include "NWay.h"

std::mutex PER_IO_DATA::bytesNumMtx;
std::condition_variable PER_IO_DATA::finishedSending;
DWORD PER_IO_DATA::bytesSent = 0;
DWORD PER_IO_DATA::bytesToSend = 0;

void NWay::initializeIOCP()
{
    _iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);
    if (_iocp == NULL) {
        std::cout << std::string("IOCP initialization failed ") + std::to_string(WSAGetLastError()) + "\n";
        throw UninitiatedValue();
    }
    _iocpFromOutsideWorkerThreads = new std::thread(&NWay::iocpWorkerThread, this);
}

void NWay::closeIOCP()
{
    if (!PostQueuedCompletionStatus(_iocp, 0, 0, 0)) {
        std::cout << std::to_string(WSAGetLastError()) + "\n";
        getchar();
    }
    _iocpFromOutsideWorkerThreads->join();
    delete _iocpFromOutsideWorkerThreads;
    _iocpFromOutsideWorkerThreads = nullptr;

    CloseHandle(_iocp);
    _iocp = NULL;
    for (auto it : _socketData) {
        delete it.second.perSocketData;
        delete it.second.readData;
        delete it.second.writeData;
    }
}

void NWay::iocpWorkerThread()
{
    DWORD bytesTransferred, fillIn, flags;
    PER_SOCKET_DATA* perSocketData = nullptr;
    PER_IO_DATA* perIOData = nullptr;
    while (GetQueuedCompletionStatus(_iocp, &bytesTransferred, (PULONG_PTR)&perSocketData, (LPOVERLAPPED*)&perIOData, INFINITE)) {//TODO free memory on exit
        if (perSocketData == 0) {
            break;
        }
        else if (perSocketData->isShutdown) {
            PER_IO_DATA::bytesSent = PER_IO_DATA::bytesToSend;
            PER_IO_DATA::finishedSending.notify_one();
            continue;
        }
        else if (bytesTransferred == 0 && perIOData->isRead) {
            perSocketData->isShutdown = true;
            std::unique_lock<std::mutex> connectionsLck(_connectionsMtx);
            closeOutsideConnection(perSocketData->token);
            connectionsLck.unlock();
            continue;
        }

        if (perIOData->isRead) {
            std::vector<byte> toSend(perIOData->dataBuf.buf, perIOData->dataBuf.buf + bytesTransferred), tokenBytes = Utilities::IntegerTypeTobigEndianBytes<uint64_t>(perSocketData->token);
            std::cout << std::string("BACKWARDED MSG 2 ") + std::to_string((int)MSG_TYPE_MEANING::OUTSIZE_MSG) + " " + std::to_string(toSend.size())+"\n";
            toSend.insert(toSend.begin(), tokenBytes.begin(), tokenBytes.end());
            _prevNode->sendMsg(toSend, (byte)MSG_TYPE_MEANING::OUTSIZE_MSG);
            flags = 0;
            ZeroMemory(&(perIOData->overlapped), sizeof(OVERLAPPED));
            perIOData->dataBuf.len = BUFFER_SIZE;
            perIOData->dataBuf.buf = perIOData->buffer;

            if (WSARecv(perSocketData->s, &(perIOData->dataBuf), 1, &fillIn, &flags, &(perIOData->overlapped), NULL) == SOCKET_ERROR && WSAGetLastError() != ERROR_IO_PENDING) {
                printf("WSARecv() failed with error %d", WSAGetLastError());
            }
        }
        else {
            std::unique_lock<std::mutex> bytesNumLck(PER_IO_DATA::bytesNumMtx);
            PER_IO_DATA::bytesSent += bytesTransferred;
            if (PER_IO_DATA::bytesSent == PER_IO_DATA::bytesToSend) {
                PER_IO_DATA::finishedSending.notify_one();
                continue;
            }

            perIOData->dataBuf.buf = perIOData->buffer + PER_IO_DATA::bytesSent;
            perIOData->dataBuf.len = PER_IO_DATA::bytesToSend - PER_IO_DATA::bytesSent;
            bytesNumLck.unlock();

            ZeroMemory(&(perIOData->overlapped), sizeof(OVERLAPPED));

            if (WSASend(perSocketData->s, &(perIOData->dataBuf), 1, &fillIn, 0, &(perIOData->overlapped), NULL) == SOCKET_ERROR && WSAGetLastError() != ERROR_IO_PENDING) {
                printf("WSASend() failed with error %d", WSAGetLastError());
            }
        }
    }
}

bool NWay::getIPAndPort(std::vector<byte> msgReceived)
{
    std::unique_lock<std::mutex> connectionsLck(_connectionsMtx);

    std::vector<byte> destIP = std::vector<byte>(msgReceived.begin(), msgReceived.begin() + IP_LEN);
    unsigned short destPort = Utilities::bigEndianBytesToIntegerType<unsigned short>(std::vector<byte>(msgReceived.begin() + IP_LEN, msgReceived.begin() + IP_LEN + PORT_LEN));
    
    _uninitiatedConnections[_nextConnID] = { destIP, destPort };

    std::cout << std::string("BACKWARDED MSG 2 ") + std::to_string((byte)MSG_TYPE_MEANING::CONN_CREATION_INITIAL) + "\n";
    
    return _prevNode->sendMsg(std::vector<byte>(Utilities::IntegerTypeTobigEndianBytes(_nextConnID)), (byte)MSG_TYPE_MEANING::CONN_CREATION_INITIAL);
}

bool NWay::initiateConnection(std::vector<byte> msgReceived)
{
    uint64_t connID = Utilities::bigEndianBytesToIntegerType<uint64_t>(msgReceived);
    ConnInfo destInfo = _uninitiatedConnections[connID];
    _uninitiatedConnections.erase(connID);
    std::pair<SOCKET, byte> addConnReturned = addConn(destInfo);
    std::vector<byte> toSend = { addConnReturned.second };
    std::unique_lock<std::mutex> connectionsLck(_connectionsMtx);

    if (addConnReturned.second != (byte)ConnectionCreationReturnValue::SUCCESS) {
        return true;
    }

    PER_SOCKET_DATA* perSocketData = new PER_SOCKET_DATA{ addConnReturned.first, _nextConnID };

    if (CreateIoCompletionPort((HANDLE)addConnReturned.first, _iocp, (DWORD)perSocketData, 0) == NULL) {
        std::cout << std::string("CreateIOCompletionPort failed with error ") + std::to_string(WSAGetLastError()) + "\n";
        throw UninitiatedValue();
    }
    PER_IO_DATA* readData = new PER_IO_DATA(), * writeData = new PER_IO_DATA();
    readData->isRead = true;
    writeData->isRead = false;

    std::unique_lock<std::mutex> socketDataLck(_socketDataMtx);
    _socketData.insert({ addConnReturned.first, SOCKET_ALL_DATA { perSocketData, readData, writeData } });
    socketDataLck.unlock();

    DWORD recvBytes, flags = 0;
    readData->dataBuf.buf = readData->buffer;
    readData->dataBuf.len = BUFFER_SIZE;
    if (WSARecv(addConnReturned.first, &(readData->dataBuf), 1, &recvBytes, &flags, &(readData->overlapped), NULL) == SOCKET_ERROR && WSAGetLastError() != ERROR_IO_PENDING) {
        std::cout << std::string("Failed at WSARecv error code ") + std::to_string(WSAGetLastError()) + "\n";
        throw UninitiatedValue();
    }
    _connections.insert({ _nextConnID, addConnReturned.first });
    _connectionsAdditionCV.notify_one();

    std::cout << std::string("BACKWARDED MSG 2 ") + std::to_string((byte)MSG_TYPE_MEANING::CONN_CREATION_CONFIRMATION) + "\n";

    return _prevNode->sendMsg(toSend, (byte)MSG_TYPE_MEANING::CONN_CREATION_CONFIRMATION);
}

bool NWay::handleConnClosureMsg(std::vector<byte> msgReceived)
{
    uint64_t connID = Utilities::bigEndianBytesToIntegerType<uint64_t>(msgReceived);

    std::unique_lock<std::mutex> connectionsLck(_connectionsMtx);
    if (_connections.find(connID) == _connections.end()) {
        return true;
    }
    shutdown(_connections[connID], SD_SEND);
    return true;
}

void NWay::handleRecvFromPrevThread()
{
    std::array<byte, 2> flags = {};
    std::vector<byte> msgReceived;
    std::unique_lock<std::mutex> connectionsLck(_connectionsMtx, std::defer_lock);
    while (!_endClass && _prevNode->recvOne(msgReceived, flags)) {
        std::cout << std::string("FORWARDED MSG 2 ") + std::to_string(flags[MSG_TYPE] != (byte)MSG_TYPE_MEANING::OUTSIZE_MSG ? flags[MSG_TYPE] : msgReceived[MSG_TYPE]) +" " + std::to_string(msgReceived.size()) + "\n";

        if (flags[MSG_TYPE] == (byte)MSG_TYPE_MEANING::CONN_CREATION_INITIAL) {//if its a connCreationInitial
            if (!getIPAndPort(msgReceived)) {
                break;
            }
        }
        else if (flags[MSG_TYPE] == (byte)MSG_TYPE_MEANING::CONN_CREATION_CONFIRMATION) {//if its a connCreationConfirmation
            if (!initiateConnection(msgReceived)) {
                break;
            }
        }
        else if (flags[MSG_TYPE] == (byte)MSG_TYPE_MEANING::CONNECTION_CLOSURE_MSG) {//if its a connClosureMsg, close the connection
            if (!handleConnClosureMsg(msgReceived)) {
                break;
            }
        }
        else if (flags[MSG_TYPE] == (byte)MSG_TYPE_MEANING::PATH_CLOSURE_MSG) {//if its a pathClosureMsgBit, close the path
            std::cout << "PATH CLOSURE MSG ARRIVED 2\n";
            _prevNode->sendMsg(std::vector<byte>(), (byte)MSG_TYPE_MEANING::PATH_CLOSURE_MSG);
            break;
        }
        else if (flags[MSG_TYPE] == (byte)MSG_TYPE_MEANING::OUTSIZE_MSG) {
            uint64_t msgID = Utilities::bigEndianBytesToIntegerType<uint64_t>(msgReceived);
            connectionsLck.lock();
            std::vector<byte> dataToSend = std::vector<byte>(msgReceived.begin() + CONN_ID_LEN, msgReceived.end());
            if (_connections.find(msgID) != _connections.end()) {//if the connection is found
                std::unique_lock<std::mutex> socketDataLck(_socketDataMtx);
                PER_IO_DATA* writeData = _socketData[_connections[msgID]].writeData;
                socketDataLck.unlock();
                do {
                    char* msgToSend = (char*)dataToSend.data();
                    std::unique_lock<std::mutex> bytesNumLck(PER_IO_DATA::bytesNumMtx);
                    connectionsLck.unlock();
                    PER_IO_DATA::finishedSending.wait(bytesNumLck, [] { return PER_IO_DATA::bytesToSend == PER_IO_DATA::bytesSent; });
                    connectionsLck.lock();
                    PER_IO_DATA::bytesSent = 0;
                    PER_IO_DATA::bytesToSend = dataToSend.size() > BUFFER_SIZE ? BUFFER_SIZE : dataToSend.size();
                    bytesNumLck.unlock();
                    DWORD fillIn;

                    ZeroMemory(&(writeData->overlapped), sizeof(OVERLAPPED));
                    memcpy(writeData->buffer, msgToSend, writeData->bytesToSend);
                    writeData->dataBuf.buf = writeData->buffer;
                    writeData->dataBuf.len = PER_IO_DATA::bytesToSend;

                    if (WSASend(_connections[msgID], &(_socketData[_connections[msgID]].writeData->dataBuf), 1, &fillIn, 0, &(_socketData[_connections[msgID]].writeData->overlapped), NULL) == SOCKET_ERROR
                        && WSAGetLastError() != ERROR_IO_PENDING) {
                        closeOutsideConnection(msgID);
                    }
                    dataToSend = std::vector<byte>(dataToSend.begin() + PER_IO_DATA::bytesToSend, dataToSend.end());
                } while (dataToSend.size() != 0);
            }
            else {
                std::cout << "A message that was received isn't to a token in _connections\n";
            }
            connectionsLck.unlock();
        }
    }
    shutdownClass(true);
}

std::pair<SOCKET, byte> NWay::addConn(ConnInfo destInfo)
{
    SOCKET newSocket;
    byte toReturn = connectSocket(destInfo, &newSocket);
    return { newSocket, toReturn };
}

byte NWay::connectSocket(ConnInfo destInfo, SOCKET* newSock)
{
    struct sockaddr_in serv_addr;
    if ((*newSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)) < 0)
    {
        std::cout << WSAGetLastError() << std::endl;
        return (byte)ConnectionCreationReturnValue::SOCKET_INITIALIZATION_FAILED;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(destInfo.port);

    // Convert IPv4 and IPv6 addresses from text to binary form 
    if (inet_pton(AF_INET, (PCSTR)Utilities::join(destInfo.IP, ".").c_str(), &serv_addr.sin_addr) == 0)//the IP sent isn't valid
    {
        std::cout << WSAGetLastError() << std::endl;
        return (byte)ConnectionCreationReturnValue::INVALID_IP;
    }

    if (connect(*newSock, (struct sockaddr*) & serv_addr, sizeof(serv_addr)) == SOCKET_ERROR)//no socket was receiving in the target on the correct port
    {
        std::cout << WSAGetLastError() << std::endl;
        return (byte)ConnectionCreationReturnValue::CONNECTION_FAILED;
    }

    return (byte)ConnectionCreationReturnValue::SUCCESS;
}

void NWay::closeOutsideConnection(uint64_t key)
{
    if (_connections.find(key) == _connections.end()) {
        return;
    }
    std::cout << "SHUTDOWN SUCCESSFULL\n";

    shutdown(_connections[key], SD_SEND);
    closesocket(_connections[key]);
    _connections.erase(key);

    std::cout << std::string("BACKWARDED MSG 2 ") + std::to_string((int)MSG_TYPE_MEANING::CONNECTION_CLOSURE_MSG) + "\n";
    _prevNode->sendMsg(Utilities::IntegerTypeTobigEndianBytes<uint64_t>(key), (byte)MSG_TYPE_MEANING::CONNECTION_CLOSURE_MSG);
}

NWay::NWay(SOCKET prevNode, std::function<void()> exitCall) : _exitCall(exitCall)
{
    std::cout << "PATH GENERATION CMPLT\n";
    initializeIOCP();
    _prevNode = new ConnectionLeft(prevNode);
	_handleRecvFromPrevThread = new std::thread(&NWay::handleRecvFromPrevThread, this);
}

NWay::NWay(ConnectionLeft* prevNode, std::function<void()> exitCall): _prevNode(prevNode), _exitCall(exitCall)
{
    std::cout << "PATH GENERATION CMPLT\n";
    initializeIOCP();
    _handleRecvFromPrevThread = new std::thread(&NWay::handleRecvFromPrevThread, this);
}

void NWay::switchSocket(SOCKET s)
{
    _prevNode->newSocket(s);
}

NWay::~NWay()
{
    shutdownClass();
    std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx);//shutdownClass finished shutting everything down
    finishShutdownLck.unlock();
    if (_iocpFromOutsideWorkerThreads != nullptr) {
        _iocpFromOutsideWorkerThreads->join();
        delete _iocpFromOutsideWorkerThreads;
    }
    if (_handleRecvFromPrevThread != nullptr) {
        _handleRecvFromPrevThread->join();
        delete _handleRecvFromPrevThread;
    }
}

void NWay::shutdownClass(bool calledFromPrevThread)
{
    std::unique_lock<std::mutex> endClassLck(_endClassMtx);//make sure no two threads enter the bracket at once
    if (!_endClass) {
        _endClass = true;
        std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx);
        endClassLck.unlock();
        _prevNode->shutdownClass();

        _connectionsAdditionCV.notify_all();
        if (!calledFromPrevThread) {//don't want to join if prev called this
            _handleRecvFromPrevThread->join();
            delete _handleRecvFromPrevThread;
            _handleRecvFromPrevThread = nullptr;
        }

        closeIOCP();
        
        for (std::pair<uint64_t, SOCKET> connection : _connections) {
            shutdown(connection.second, SD_SEND);
            closesocket(connection.second);
        }
        _exitCall();
        delete _prevNode;
    }
}
