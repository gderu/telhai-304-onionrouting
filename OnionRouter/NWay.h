#pragma once
#include <algorithm>
#include <iterator>
#include <functional>
#include <iostream>
#include "ConnInfo.h"
#include "ConnectionLeft.h"
#include "ConnectionCreationReturnValue.h"
#define IP_LEN 4
#define PORT_LEN 2
#define CONN_ID_LEN 8
#define BUFFER_SIZE 2041

struct PER_SOCKET_DATA {
	SOCKET s;
	uint64_t token;
	int isShutdown = false;
};

struct PER_IO_DATA {
	OVERLAPPED overlapped;
	WSABUF dataBuf;
	CHAR buffer[BUFFER_SIZE];
	BOOL isRead;
	static std::mutex bytesNumMtx;
	static DWORD bytesSent;
	static DWORD bytesToSend;
	static std::condition_variable finishedSending;
};

struct SOCKET_ALL_DATA {
	PER_SOCKET_DATA *perSocketData;
	PER_IO_DATA *readData;
	PER_IO_DATA *writeData;
};

class NWay
{
	uint64_t _nextConnID = 0;
	std::mutex _connectionsMtx, _endClassMtx, _finishShutdownMtx, _socketDataMtx;
	std::condition_variable _connectionsAdditionCV;
	ConnectionLeft* _prevNode;
	std::map<uint64_t, ConnInfo> _uninitiatedConnections;
	std::map<uint64_t, SOCKET> _connections;
	std::map<SOCKET, SOCKET_ALL_DATA> _socketData;
	std::atomic<bool> _endClass = false;
	std::function<void()> _exitCall;
	HANDLE _iocp;

	std::thread *_handleRecvFromPrevThread;
	std::thread* _iocpFromOutsideWorkerThreads;

	void initializeIOCP();
	void closeIOCP();
	void iocpWorkerThread();

	bool getIPAndPort(std::vector<byte> msgReceived);
	bool initiateConnection(std::vector<byte> msgReceived);
	bool handleConnClosureMsg(std::vector<byte> msgReceived);
	void handleRecvFromPrevThread();//handles the messages received from the previous node and passes them to the outside
	std::pair<SOCKET, byte> addConn(ConnInfo destInfo);//THROWS UninitiatedValue. Adds a conn to _connections
	byte connectSocket(ConnInfo destInfo, SOCKET* newSock);//sets the socket to connect with destInfo
	void closeOutsideConnection(uint64_t key);//close a certain connection with the outside. _connectionsMtx must be locked when this is called
public:
	NWay(SOCKET prevNode, std::function<void()> exitCall);
	NWay(ConnectionLeft* prevNode, std::function<void()> exitCall);
	~NWay();

	void shutdownClass(bool calledFromPrevThread = false);
	void switchSocket(SOCKET s);
};

