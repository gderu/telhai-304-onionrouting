#pragma once
#include <functional>
#include "ConnectionLeft.h"
#include "ConnectionRight.h"

class TwoWay
{
	ConnectionLeft* _prevNode;
	ConnectionRight _nextNode;
	std::thread *_handleRecvFromPrevThread, * _handleRecvFromNextThread;
	std::function<void()> _exitCall;
	NODE_NAMES _nodeName;
	std::atomic<bool> _endClass;
	std::mutex _endClassMtx, _finishShutdownMtx;

	void handleRecvFromPrevThread();//handles the messages received from the previous node and passes them to the next node
	void handleRecvFromNextThread();//handles the messages received from the next node and passes them to the previous node
	bool getMsg(UnprocessedMsg& um);//gets a message from nextNode
public:
	TwoWay(ConnectionLeft* prevNode, std::vector<byte> nextNodeIP, std::function<void()> exitCall);
	~TwoWay();
	void shutdownClass(bool prevCalled = false, bool nextCalled = false);//shuts the class down
	void switchSocket(SOCKET s);//switches the socket to a new one
};

