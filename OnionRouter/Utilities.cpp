#include "Utilities.h"

std::vector<std::string> Utilities::split(std::string s, std::string delimeter)
{
	size_t pos = 0;
	std::vector<std::string> toReturn;
	while ((pos = s.find(delimeter)) != std::string::npos) {//finds the next index of delimeter, get a substring until it, then erases said substring as well as the delimeter
		toReturn.push_back(s.substr(0, pos));
		s.erase(0, pos + delimeter.length());
	}
	toReturn.push_back(s);
	return toReturn;
}

bool Utilities::isIPV4InternetConnectionOk()
{
	CoInitialize(NULL);//ensuring there is an internet connection
	{
		CComPtr<INetworkListManager> pNLM;
		HRESULT hr = CoCreateInstance(CLSID_NetworkListManager, NULL,
			CLSCTX_ALL, __uuidof(INetworkListManager), (LPVOID*)&pNLM);
		if (SUCCEEDED(hr))
		{
			NLM_CONNECTIVITY con = NLM_CONNECTIVITY_DISCONNECTED;
			hr = pNLM->GetConnectivity(&con);
			if SUCCEEDED(hr)
			{
				if (con & NLM_CONNECTIVITY_IPV4_INTERNET) {//TODO ensure this works/
					return true;
				}
			}
		}
	}
	CoUninitialize();
	return false;
}

byte Utilities::isolateBit(byte aByte, byte byteToIsolate)
{
	aByte = (aByte << (BITS_FROM_SIGNIFIANT_TO_LEAST - byteToIsolate));
	return aByte >> BITS_FROM_SIGNIFIANT_TO_LEAST;
}

std::string Utilities::join(std::vector<byte> vec, std::string delimeter) {
	std::string toReturn;
	for (byte part : vec) {
		toReturn += std::to_string(part) + delimeter;
	}
	toReturn = toReturn.substr(0, toReturn.size() - delimeter.size());
	return toReturn;
}

byte Utilities::setBit(byte original, byte index, bool val)
{
	return val ? original | 1UL << index : original & ~(1UL << index);
}

bool Utilities::getBit(byte b, byte index)
{
	return (b & (1 << index)) >> index;
}
