#pragma once
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <atlbase.h>
#include <Netlistmgr.h>
#include "Exceptions.h"
#define LEAST_SIGNIFICANT_BYTE_SHORT_INT 1
#define VALUES_IN_BYTE 255
#define BITS_FROM_SIGNIFIANT_TO_LEAST 7

typedef unsigned char byte;

class ConnectionLeft;

class Utilities
{
public:

	//THROWS LackingBytes if less than the length of T was given!
	//will only refer to the first sizeof(T) bytes/
	template <class T>
	static inline T bigEndianBytesToIntegerType(std::vector<byte> n)
	{
		if (n.size() < sizeof(T)) {
			throw(LackingBytes());
		}
		T toReturn = 0;
		for (int i = 0; i < sizeof(T); i++) {
			toReturn |= ((T)n[i]) << ((sizeof(T) - i - 1) * 8);
		}
		return toReturn;
	}

	template <class T>
	static inline std::vector<byte> IntegerTypeTobigEndianBytes(T n)
	{
		std::vector<byte> res(sizeof(T));
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			res[i] = (byte)(n - (n >> 8 << 8));
			n = n >> 8;
		}
		return res;
	}


	static std::vector<std::string> split(std::string s, std::string delimeter);

	static std::string join(std::vector<byte> vec, std::string delimeter);

	static byte setBit(byte original, byte index, bool val);

	static bool getBit(byte b, byte index);

	static bool isIPV4InternetConnectionOk();

	template <class Key, class Value>
	static std::pair<std::vector<Key>, std::vector<Value>> splitMap(std::map<Key, Value> mapInput) {
		std::vector<Key> keyVec;
		std::vector<Value> valVec;
		for (auto it = mapInput.begin(); it != mapInput.end(); it++) {
			keyVec.push_back(it->first);
			valVec.push_back(it->second);
		}
		return { keyVec, valVec };
	}

	//byteToISolate starts from the lest significant (0) to the most significant (7)
	static byte isolateBit(byte aByte, byte byteToIsolate);


};

